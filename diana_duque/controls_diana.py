from helper.helper import Helper


def test_g_acierta_score(g_acierta, score_acierta_gf):
    if g_acierta == "G1":
        return score_acierta_gf >= 970
    elif g_acierta == "G2":
        return score_acierta_gf < 970 and score_acierta_gf >= 890
    elif g_acierta == "G3":
        return score_acierta_gf < 890 and score_acierta_gf >= 835
    elif g_acierta == "G4":
        return score_acierta_gf < 835 and score_acierta_gf >= 755
    elif g_acierta == "G5":
        return score_acierta_gf < 755 and score_acierta_gf >= 700
    elif g_acierta == "G6":
        return score_acierta_gf < 700 and score_acierta_gf >= 590
    elif g_acierta == "G7":
        return score_acierta_gf < 590 and score_acierta_gf >= 50
    elif g_acierta is None:
        return score_acierta_gf < 50


def test_fte_riesgo(modelo_evaluado, g_comportamiento, g_sin_exp, g_acierta, grupo_de_riesgo, fte_grupo_de_riesgo):
    if g_comportamiento is not None:
        return modelo_evaluado == "MODELO COMPORTAMIENTO" and g_comportamiento == grupo_de_riesgo and fte_grupo_de_riesgo == "G COMPORTAMIENTO"
    elif modelo_evaluado in ("MODELO COMPLETO", "MODELO NFI-EXT"):
        return g_sin_exp is not None and g_sin_exp == grupo_de_riesgo and fte_grupo_de_riesgo == "G SIN EXPERIENCIA"
    elif g_acierta is not None and g_acierta != "G7":
        return modelo_evaluado == "MODELO ACIERTA" and g_acierta == grupo_de_riesgo and fte_grupo_de_riesgo == "G ACIERTA"
    elif g_sin_exp is not None:
        return (modelo_evaluado in ("MODELO NFI-TRX", "MODELO NFI")) and g_sin_exp == grupo_de_riesgo and fte_grupo_de_riesgo == "G SIN EXPERIENCIA"
    elif g_acierta == "G7":
        return modelo_evaluado == "MODELO ACIERTA" and g_acierta == grupo_de_riesgo and fte_grupo_de_riesgo == "G ACIERTA"
    else:
        return modelo_evaluado is None and grupo_de_riesgo is None and fte_grupo_de_riesgo is None


params = {
    "tabla_resultados": "proceso_riesgos.nei_base_completa_perfilacion_2020_q1",
    "tabla_buro": "resultados_preaprobados.buro_indep_sineeff_consolidado",
    "nxcaso": "2",
    "excel_sal": "controles.xlsx"
}

imp = Helper()

params["buro_ing"] = imp.obtener_ultima_ingestion(params["tabla_buro"])
imp.ejecutar_archivo("control_queries.sql", params)
con_df = imp.obtener_dataframe("SELECT * FROM proceso.control_calif")
con_df["score_corresponde_g_acierta"] = con_df.apply(
    lambda x: test_g_acierta_score(x['g_acierta'], x['score_acierta_gf']), axis=1)
con_df["modelo_correcto"] = con_df.apply(lambda x: test_fte_riesgo(
    x['modelo_evaluado'], x['g_comportamiento'], x['g_sin_exp'], x['g_acierta'], x['grupo_de_riesgo'], x['fte_grupo_de_riesgo']), axis=1)
con_df.to_excel(params["excel_sal"], sheet_name="Control_G", index=False)
